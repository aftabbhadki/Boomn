package com.boomn;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boomn.NavigationModule.Fragments.HomeFragment;
import com.boomn.NavigationModule.Fragments.PostAddFragment;
import com.boomn.ProfileModule.Fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout ll_myaccount, ll_postads;
    DrawerLayout drawer;
    static RelativeLayout rr_bottom;
    ImageView iv_drawer;
    static TextView main_title;
    RelativeLayout rr_postads;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        /////creating home fragment////
        Fragment homefragment = new HomeFragment();
        replacefragment(homefragment);

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initViews() {

        ll_myaccount = (LinearLayout) findViewById(R.id.ll_myaccount);
        ll_myaccount.setOnClickListener(this);
        rr_bottom = (RelativeLayout) findViewById(R.id.bottom_layout);
        iv_drawer = (ImageView) findViewById(R.id.iv_drawer);
        iv_drawer.setOnClickListener(this);
        main_title = (TextView) findViewById(R.id.main_title);
        rr_postads = (RelativeLayout) findViewById(R.id.rr_postads);
        ll_postads = (LinearLayout) findViewById(R.id.ll_postads);
        rr_postads.setOnClickListener(this);
        ll_postads.setOnClickListener(this);

        // rr_bottom.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_myaccount:
                replacefragment(new ProfileFragment());
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.iv_drawer:
                drawer.openDrawer(GravityCompat.START);
                break;
            case R.id.rr_postads:
                replacefragment(new PostAddFragment());
                break;
            case R.id.ll_postads:
                replacefragment(new PostAddFragment());
                drawer.closeDrawer(GravityCompat.START);
                break;

        }
    }


    public void replacefragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment)
                .addToBackStack(fragment.getClass().getName()).commit();

    }

    public static void hidebootom() {

        rr_bottom.setVisibility(View.GONE);

    }

    public static void showbottom() {
        rr_bottom.setVisibility(View.VISIBLE);
    }

    public static void setTitle(String title) {
        main_title.setText(title);
    }


}
