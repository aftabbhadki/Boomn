package com.boomn.SLidingModule.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.boomn.NavigationModule.Models.FriendModel;
import com.boomn.R;
import com.boomn.SLidingModule.Activities.RealEstateDetailsActivity;
import com.boomn.SLidingModule.Models.RealEstateModel;

import java.util.List;

public class RealEstateAdapter extends RecyclerView.Adapter<RealEstateAdapter.MyViewHolder> implements View.OnClickListener{

    Context context;
    List<RealEstateModel> friendModelList;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_real_estate_row, parent, false);

        return new MyViewHolder(itemView);

    }

    public RealEstateAdapter(Context context, List<RealEstateModel> friendModelList) {
        this.context = context;
        this.friendModelList = friendModelList;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        RealEstateModel friendModel = friendModelList.get(position);

        //holder.tv_name.setText(friendModel.getFriendname());
        //holder.tv_email.setText(friendModel.getFriendemail());
        holder.iv_property.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return friendModelList.size();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_property:
                Intent details_activity = new Intent(context, RealEstateDetailsActivity.class);
                context.startActivity(details_activity);
                break;

        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView iv_property;
        public TextView tv_aprtmentname, tv_city,tv_postedon;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv_property = (ImageView) itemView.findViewById(R.id.iv_property);
            tv_aprtmentname = (TextView) itemView.findViewById(R.id.tv_name);
            tv_city = (TextView) itemView.findViewById(R.id.tv_city);

        }
    }


}
