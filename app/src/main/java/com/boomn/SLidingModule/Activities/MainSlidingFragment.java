package com.boomn.SLidingModule.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.boomn.R;
import com.boomn.SLidingModule.Adapters.SampleFragmentPagerAdapter;
import com.boomn.SLidingModule.Fragments.BuisnessOpportunitiesFragment;
import com.boomn.SLidingModule.Fragments.RealEstateFragment;

public class MainSlidingFragment extends Fragment {
    private TabLayout tabLayout;
    public ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.activity_main_sliding, container, false);
        initviews(view);
        return view;
    }

    private void initviews(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.custom_tab, null, false);

        LinearLayout linearLayoutOne = (LinearLayout) headerView.findViewById(R.id.ll);
        LinearLayout linearLayout2 = (LinearLayout) headerView.findViewById(R.id.ll2);
        LinearLayout linearLayout3 = (LinearLayout) headerView.findViewById(R.id.ll3);

        tabLayout.getTabAt(0).setCustomView(linearLayoutOne);
        tabLayout.getTabAt(1).setCustomView(linearLayout2);
        tabLayout.getTabAt(2).setCustomView(linearLayout3);

    }
    private void setupViewPager(ViewPager viewPager) {
        SampleFragmentPagerAdapter adapter = new SampleFragmentPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new RealEstateFragment(), "ONE");
        adapter.addFragment(new BuisnessOpportunitiesFragment(), "TWO");
        adapter.addFragment(new BuisnessOpportunitiesFragment(), "THREE");

        viewPager.setAdapter(adapter);
    }
}
