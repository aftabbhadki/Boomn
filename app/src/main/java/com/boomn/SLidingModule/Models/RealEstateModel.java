package com.boomn.SLidingModule.Models;

public class RealEstateModel {
    private String url, name, city, addeddate, property_name, profile_url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddeddate() {
        return addeddate;
    }

    public void setAddeddate(String addeddate) {
        this.addeddate = addeddate;
    }

    public String getProperty_name() {
        return property_name;
    }

    public void setProperty_name(String property_name) {
        this.property_name = property_name;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public RealEstateModel(String url, String name, String city, String addeddate, String property_name, String profile_url) {
        this.url = url;
        this.name = name;
        this.city = city;
        this.addeddate = addeddate;
        this.property_name = property_name;
        this.profile_url = profile_url;
    }
}
