package com.boomn.SLidingModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.boomn.MainActivity;
import com.boomn.R;
import com.boomn.SLidingModule.Adapters.RealEstateAdapter;
import com.boomn.SLidingModule.Models.RealEstateModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RealEstateFragment extends Fragment {
    RecyclerView realestate_list;
    List<RealEstateModel> realEstateModelList = new ArrayList<>();



    public RealEstateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_real_estate, container, false);
        initviews(view);
        return view;
    }

    private void initviews(View view) {
        MainActivity.setTitle(getResources().getString(R.string.realestate));
        realestate_list = (RecyclerView) view.findViewById(R.id.realestate_list);
        realestate_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        realEstateModelList.add(new RealEstateModel("","","","","",""));
        realEstateModelList.add(new RealEstateModel("","","","","",""));
        realEstateModelList.add(new RealEstateModel("","","","","",""));
        realEstateModelList.add(new RealEstateModel("","","","","",""));
        realestate_list.setAdapter(new RealEstateAdapter(getActivity(),realEstateModelList));
    }

}
