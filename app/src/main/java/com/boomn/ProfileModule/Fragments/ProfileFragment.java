package com.boomn.ProfileModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.boomn.MainActivity;
import com.boomn.NavigationModule.Fragments.TimelineFragment;
import com.boomn.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {

    LinearLayout ll_timeline;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        MainActivity.hidebootom();
        iniiViews(view);
        return view;
    }

    private void iniiViews(View view) {
        ll_timeline = (LinearLayout) view.findViewById(R.id.ll_timeline);
        ll_timeline.setOnClickListener(this);
        MainActivity.setTitle(getResources().getString(R.string.profile));

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ll_timeline:
                replacefragment(new TimelineFragment());
                break;
        }
    }

    public void replacefragment(Fragment fragment) {
        getChildFragmentManager().beginTransaction().replace(R.id.frame_container1, fragment)
                .addToBackStack(fragment.getClass().getName()).commit();

    }
}
