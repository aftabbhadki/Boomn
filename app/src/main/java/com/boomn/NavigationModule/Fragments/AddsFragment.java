package com.boomn.NavigationModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.boomn.NavigationModule.Adapters.AddsAdapter;
import com.boomn.NavigationModule.Models.AddsModel;
import com.boomn.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddsFragment extends Fragment {
    RecyclerView list_adds;
    List<AddsModel> addsModelList = new ArrayList<>();


    public AddsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adds, container, false);
        initviews(view);
        return view;
    }

    private void initviews(View view) {

        list_adds = (RecyclerView) view.findViewById(R.id.list_adds);
        addsModelList.add(new AddsModel("www.google.com", "Real Estate"));
        addsModelList.add(new AddsModel("www.google.com", "Real Estate"));
        addsModelList.add(new AddsModel("www.google.com", "Real Estate"));
        addsModelList.add(new AddsModel("www.google.com", "Real Estate"));
        list_adds.setLayoutManager(new LinearLayoutManager(getActivity()));
        list_adds.setAdapter(new AddsAdapter(getActivity(), addsModelList));

    }

}
