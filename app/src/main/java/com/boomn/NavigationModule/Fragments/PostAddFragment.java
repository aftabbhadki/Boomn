package com.boomn.NavigationModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.boomn.MainActivity;
import com.boomn.NavigationModule.Adapters.AddsAdapter;
import com.boomn.NavigationModule.Models.AddsModel;
import com.boomn.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostAddFragment extends Fragment {

    RecyclerView rv_adslist;
    List<AddsModel> addsAdapterList = new ArrayList<>();

    public PostAddFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post_add, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        MainActivity.setTitle(getResources().getString(R.string.choseadstype));
        rv_adslist = (RecyclerView) view.findViewById(R.id.rv_adslist);
        rv_adslist.setLayoutManager(new LinearLayoutManager(getActivity()));
        addsAdapterList.add(new AddsModel("www.google.com", getActivity().getResources().getString(R.string.realestate)));
        addsAdapterList.add(new AddsModel("www.google.com", getActivity().getResources().getString(R.string.buisnessopur)));
        addsAdapterList.add(new AddsModel("www.google.com", getActivity().getResources().getString(R.string.jobs)));
        addsAdapterList.add(new AddsModel("www.google.com", getActivity().getResources().getString(R.string.healthfood)));
        addsAdapterList.add(new AddsModel("",getActivity().getResources().getString(R.string.auto)));

        rv_adslist.setAdapter(new AddsAdapter(getActivity(),addsAdapterList));

    }

}
