package com.boomn.NavigationModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.boomn.MainActivity;
import com.boomn.NavigationModule.Adapters.HomeAdapter;
import com.boomn.NavigationModule.Interfaces.ReplaceFragment;
import com.boomn.NavigationModule.Models.HomeModel;
import com.boomn.Network.Apis;
import com.boomn.Network.MyApiRequest;
import com.boomn.Network.OnRequestCompleted;
import com.boomn.R;
import com.boomn.SLidingModule.Activities.MainSlidingFragment;
import com.boomn.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ReplaceFragment,OnRequestCompleted {
    RecyclerView list_category;
    List<HomeModel> homeModelList = new ArrayList<>();


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        MainActivity.showbottom();
        list_category = (RecyclerView) view.findViewById(R.id.list_category);
        list_category.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        homeModelList.add(new HomeModel(getActivity().getResources().getString(R.string.realestate), R.drawable.realestate));
        homeModelList.add(new HomeModel(getActivity().getResources().getString(R.string.buisnessopur), R.drawable.realestate));
        homeModelList.add(new HomeModel(getActivity().getResources().getString(R.string.healthfood), R.drawable.realestate));
        homeModelList.add(new HomeModel(getActivity().getResources().getString(R.string.jobs), R.drawable.realestate));
        homeModelList.add(new HomeModel(getActivity().getResources().getString(R.string.auto), R.drawable.realestate));
        homeModelList.add(new HomeModel(getActivity().getResources().getString(R.string.service), R.drawable.realestate));
        homeModelList.add(new HomeModel(getActivity().getResources().getString(R.string.forsale), R.drawable.realestate));
        homeModelList.add(new HomeModel(getActivity().getResources().getString(R.string.travel), R.drawable.realestate));
        list_category.setAdapter(new HomeAdapter(getActivity(), homeModelList, this));
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.setTitle(getResources().getString(R.string.home));
        MyApiRequest myApiRequest = new MyApiRequest(getActivity(),this);
        myApiRequest.executeLoopjCall(Apis.BOOMN_ABOUTUS,"");
    }

    @Override
    public void onClickView(int position) {
        switch (position) {

            case 0:
                setFragment(new MainSlidingFragment());
                break;

        }


    }

    public void setFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).
                addToBackStack(fragment.getClass().getName()).commit();

    }

    @Override
    public void taskCompleted(String results) {
        Log.d("DATA",results);

    }
}
