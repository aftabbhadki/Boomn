package com.boomn.NavigationModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.boomn.NavigationModule.Adapters.ResidentialAdapter;
import com.boomn.NavigationModule.Models.ResidentialModel;
import com.boomn.R;


import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResidentialFragment extends Fragment {
    RecyclerView list_recidential;
    List<ResidentialModel> residentialModelList = new ArrayList<>();
    


    public ResidentialFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_residential, container, false);
        initviews(view);
        return view;
    }

    private void initviews(View view) {
        list_recidential = (RecyclerView) view.findViewById(R.id.list_recidential);
        list_recidential.setLayoutManager(new LinearLayoutManager(getActivity()));
        residentialModelList.add(new ResidentialModel("bfhsfbsdhfdsb"));
        residentialModelList.add(new ResidentialModel("bfhsfbsdhfdsb"));
        residentialModelList.add(new ResidentialModel("bfhsfbsdhfdsb"));
        residentialModelList.add(new ResidentialModel("bfhsfbsdhfdsb"));
        list_recidential.setAdapter(new ResidentialAdapter(getActivity(), residentialModelList));

    }

}
