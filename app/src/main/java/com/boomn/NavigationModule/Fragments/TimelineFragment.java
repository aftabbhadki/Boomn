package com.boomn.NavigationModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.boomn.NavigationModule.Adapters.TimelineAdapter;
import com.boomn.NavigationModule.Models.TimelineModel;
import com.boomn.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimelineFragment extends Fragment {

    RecyclerView list_timelines;
    List<TimelineModel> timelineModelList = new ArrayList<>();


    public TimelineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_timeline, container, false);
        initview(view);
        // Inflate the layout for this fragment
        return view;
    }

    private void initview(View view) {
        list_timelines = (RecyclerView) view.findViewById(R.id.list_timelines);
        timelineModelList.add(new TimelineModel("www.google.com", "Auto / transportation", "mkskfsd nfsdsd fhufubsdyfb byrbyfr byrybf" +
                "bufrbfr bufrbfbf dfhsud fushufhs " +
                "hfsuhfusdh hfushdus"));
        timelineModelList.add(new TimelineModel("www.google.com", "Auto / transportation", "mkskfsd nfsdsd fhudfhsud fushufhs " +
                "hfsuhfusdh hfushdus"));
        timelineModelList.add(new TimelineModel("www.google.com", "Auto / transportation", "mkskfsd nfsdsd fhudfhsud fushufhs " +
                "hfsuhfusdh hfushdus"));
        timelineModelList.add(new TimelineModel("www.google.com", "Auto / transportation", "mkskfsd nfsdsd fhudfhsud fushufhs " +
                "hfsuhfusdh hfushdus"));
        list_timelines.setLayoutManager(new LinearLayoutManager(getActivity()));
        list_timelines.setAdapter(new TimelineAdapter(getActivity(), timelineModelList));
    }

}
