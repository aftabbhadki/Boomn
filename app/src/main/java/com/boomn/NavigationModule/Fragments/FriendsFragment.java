package com.boomn.NavigationModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.boomn.NavigationModule.Adapters.FriendsAdapter;
import com.boomn.NavigationModule.Models.FriendModel;
import com.boomn.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {

    RecyclerView list_friends;
    List<FriendModel> friendModelList = new ArrayList<>();


    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        // Inflate the layout for this fragment
        initviews(view);
        return view;
    }

    private void initviews(View view) {
        list_friends = (RecyclerView) view.findViewById(R.id.list_friends);
        list_friends.setLayoutManager(new LinearLayoutManager(getActivity()));
        friendModelList.add(new FriendModel("Mr. Aftab Bhadki", "bhadkiaftab@gmail.com", "googgle.com"));
        friendModelList.add(new FriendModel("Mr. Aftab Bhadki", "bhadkiaftab@gmail.com", "googgle.com"));
        friendModelList.add(new FriendModel("Mr. Aftab Bhadki", "bhadkiaftab@gmail.com", "googgle.com"));
        friendModelList.add(new FriendModel("Mr. Aftab Bhadki", "bhadkiaftab@gmail.com", "googgle.com"));
        list_friends.setLayoutManager(new LinearLayoutManager(getActivity()));
        list_friends.setAdapter(new FriendsAdapter(getActivity(), friendModelList));
    }

}
