package com.boomn.NavigationModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.boomn.NavigationModule.Adapters.TemplateAdapter;
import com.boomn.NavigationModule.Models.TemplateModule;
import com.boomn.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TemplateFragment extends Fragment {
    RecyclerView template_list;
    List<TemplateModule> templateModuleList = new ArrayList<>();


    public TemplateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_template, container, false);   // Inflate the layout for this fragment
        initview(view);
        return view;
    }

    private void initview(View view) {
        template_list = (RecyclerView) view.findViewById(R.id.template_list);
        template_list.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        templateModuleList.add(new TemplateModule("www.google.com"));
        templateModuleList.add(new TemplateModule("www.google.com"));
        templateModuleList.add(new TemplateModule("www.google.com"));
        template_list.setAdapter(new TemplateAdapter(getActivity(),templateModuleList));
    }

}
