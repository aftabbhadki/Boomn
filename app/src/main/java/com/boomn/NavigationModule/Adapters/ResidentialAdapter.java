package com.boomn.NavigationModule.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.boomn.NavigationModule.Models.ResidentialModel;
import com.boomn.R;

import java.util.List;

public class ResidentialAdapter extends RecyclerView.Adapter<ResidentialAdapter.MyViewHolder> {

    Context context;
    List<ResidentialModel> residentialModelList;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_resdentialtype, parent, false);

        return new MyViewHolder(itemView);

    }

    public ResidentialAdapter(Context context, List<ResidentialModel> residentialModelList) {
        this.context = context;
        this.residentialModelList = residentialModelList;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ResidentialModel residentialModel = residentialModelList.get(position);
        holder.residentialtype.setText(residentialModel.getResidentialtype());


    }

    @Override
    public int getItemCount() {
        return residentialModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView residentialtype;

        public MyViewHolder(View itemView) {
            super(itemView);
            //imageView = (ImageView) itemView.findViewById(R.id.iv_source);
            residentialtype = (TextView) itemView.findViewById(R.id.tv_residentialtype);


        }
    }


}
