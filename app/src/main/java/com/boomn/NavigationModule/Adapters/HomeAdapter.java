package com.boomn.NavigationModule.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boomn.NavigationModule.Fragments.HomeFragment;
import com.boomn.NavigationModule.Interfaces.ReplaceFragment;
import com.boomn.NavigationModule.Models.HomeModel;
import com.boomn.R;
import com.boomn.SLidingModule.Activities.MainSlidingFragment;

import java.util.List;


/**
 * Created by test on 20/01/18.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    private List<HomeModel> imagesModelListList;
    Context context;
    ReplaceFragment replaceFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, experiece, subject;
        public TextView action_more;
        public RelativeLayout rr_action;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tv_categoryname);
            rr_action = (RelativeLayout) view.findViewById(R.id.rr_action);

        }
    }


    public HomeAdapter(Context context, List<HomeModel> imagesModelListList, ReplaceFragment replaceFragment) {
        this.imagesModelListList = imagesModelListList;
        this.context = context;
        this.replaceFragment = replaceFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_home, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        HomeModel icategories = imagesModelListList.get(position);
        holder.name.setText(icategories.getCategoryname());
        holder.rr_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment.onClickView(position);


            }
        });


    }

    @Override
    public int getItemCount() {
        return imagesModelListList.size();
    }
}
