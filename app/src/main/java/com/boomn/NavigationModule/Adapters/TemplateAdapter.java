package com.boomn.NavigationModule.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.boomn.NavigationModule.Models.TemplateModule;
import com.boomn.R;

import java.util.List;

public class TemplateAdapter extends RecyclerView.Adapter<TemplateAdapter.MyViewHolder> {

    Context context;
    List<TemplateModule> templateModuleList;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_adds, parent, false);

        return new MyViewHolder(itemView);

    }

    public TemplateAdapter(Context context, List<TemplateModule> templateModuleList) {
        this.context = context;
        this.templateModuleList = templateModuleList;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TemplateModule addsModel = templateModuleList.get(position);
        //holder.tv_addname.setText(addsModel.getAdd_name());


    }

    @Override
    public int getItemCount() {
        return templateModuleList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView tv_addname;

        public MyViewHolder(View itemView) {
            super(itemView);
            //imageView = (ImageView) itemView.findViewById(R.id.iv_source);
            //tv_addname = (TextView) itemView.findViewById(R.id.tv_addname);


        }
    }


}
