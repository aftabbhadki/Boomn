package com.boomn.NavigationModule.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.boomn.NavigationModule.Models.FriendModel;
import com.boomn.R;

import java.util.List;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.MyViewHolder> {

    Context context;
    List<FriendModel> friendModelList;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_friends, parent, false);

        return new MyViewHolder(itemView);

    }

    public FriendsAdapter(Context context, List<FriendModel> friendModelList) {
        this.context = context;
        this.friendModelList = friendModelList;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        FriendModel friendModel = friendModelList.get(position);

        holder.tv_name.setText(friendModel.getFriendname());
        holder.tv_email.setText(friendModel.getFriendemail());

    }

    @Override
    public int getItemCount() {
        return friendModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView tv_name, tv_email;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.profile_logo);
            tv_name = (TextView) itemView.findViewById(R.id.tv_friendname);
            tv_email = (TextView) itemView.findViewById(R.id.tv_friendemail);

        }
    }


}
