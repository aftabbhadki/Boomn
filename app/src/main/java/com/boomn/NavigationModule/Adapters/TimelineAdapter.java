package com.boomn.NavigationModule.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.boomn.NavigationModule.Models.TimelineModel;
import com.boomn.R;

import java.util.List;

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.MyViewHolder> {

    Context context;
    List<TimelineModel> timelineModelList;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_timeline, parent, false);

        return new MyViewHolder(itemView);

    }

    public TimelineAdapter(Context context, List<TimelineModel> timelineModelList) {
        this.context = context;
        this.timelineModelList = timelineModelList;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TimelineModel timelineModel = timelineModelList.get(position);

        holder.tv_industrytype.setText(timelineModel.getSelecttype());
        holder.tv_description.setText(timelineModel.getDescription());

    }

    @Override
    public int getItemCount() {
        return timelineModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView tv_industrytype, tv_description;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.iv_source);
            tv_industrytype = (TextView) itemView.findViewById(R.id.tv_industrytype);
            tv_description = (TextView) itemView.findViewById(R.id.tv_description);

        }
    }


}
