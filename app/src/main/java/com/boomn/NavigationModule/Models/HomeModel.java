package com.boomn.NavigationModule.Models;

/**
 * Created by mahadevi on 11-04-2018.
 */

public class HomeModel {
    private String categoryname;
    private int image;

    public HomeModel(String categoryname, int image) {
        this.categoryname = categoryname;
        this.image = image;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
