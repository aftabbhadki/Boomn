package com.boomn.NavigationModule.Models;

public class TimelineModel {
    private String url,selecttype,description;

    public TimelineModel(String url, String selecttype, String description) {
        this.url = url;
        this.selecttype = selecttype;
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSelecttype() {
        return selecttype;
    }

    public void setSelecttype(String selecttype) {
        this.selecttype = selecttype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
