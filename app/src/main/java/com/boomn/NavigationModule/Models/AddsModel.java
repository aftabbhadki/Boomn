package com.boomn.NavigationModule.Models;

public class AddsModel {
    private String url,add_name;

    public AddsModel(String url, String add_name) {
        this.url = url;
        this.add_name = add_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAdd_name() {
        return add_name;
    }

    public void setAdd_name(String add_name) {
        this.add_name = add_name;
    }
}
