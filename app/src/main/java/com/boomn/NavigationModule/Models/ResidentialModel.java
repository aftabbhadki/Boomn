package com.boomn.NavigationModule.Models;

public class ResidentialModel {
    private String residentialtype;

    public ResidentialModel(String residentialtype) {
        this.residentialtype = residentialtype;
    }

    public String getResidentialtype() {
        return residentialtype;
    }

    public void setResidentialtype(String residentialtype) {
        this.residentialtype = residentialtype;
    }
}
