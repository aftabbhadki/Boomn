package com.boomn.NavigationModule.Models;

public class FriendModel {
    private String friendname,friendemail,url;

    public FriendModel(String friendname, String friendemail, String url) {
        this.friendname = friendname;
        this.friendemail = friendemail;
        this.url = url;
    }

    public String getFriendname() {
        return friendname;
    }

    public void setFriendname(String friendname) {
        this.friendname = friendname;
    }

    public String getFriendemail() {
        return friendemail;
    }

    public void setFriendemail(String friendemail) {
        this.friendemail = friendemail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
