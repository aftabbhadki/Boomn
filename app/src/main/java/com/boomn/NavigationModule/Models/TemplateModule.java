package com.boomn.NavigationModule.Models;

public class TemplateModule {
    private String url;

    public TemplateModule(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
