package com.boomn.Network;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MyApiRequest {
    private static final String TAG = "MOVIE_TRIVIA";

    AsyncHttpClient asyncHttpClient;
    RequestParams requestParams;
    Context context;
    OnRequestCompleted onRequestCompleted;

    //String BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.3072,73.1812&radius=400&name=Vadodara&key=AIzaSyBMxSqACbLtQeTsu0d-2ExKIdfzVuxMPv8";
    String jsonResponse;

    public MyApiRequest(Context context,OnRequestCompleted onRequestCompleted) {
        asyncHttpClient = new AsyncHttpClient();
        requestParams = new RequestParams();
        this.context = context;
        this.onRequestCompleted = onRequestCompleted;
    }

    public void executeLoopjCall(String url,String queryTerm) {
        //requestParams.put("s", queryTerm);
        asyncHttpClient.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                jsonResponse = response.toString();
                onRequestCompleted.taskCompleted(jsonResponse);
                Log.i(TAG, "onSuccess: " + jsonResponse);
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.e(TAG, "onFailure: " + errorResponse);
            }
        });
    }
    public void executePostrequest(String url,RequestParams requestParams){

        asyncHttpClient.post(url,requestParams,new JsonHttpResponseHandler(){



        });
    }


}
