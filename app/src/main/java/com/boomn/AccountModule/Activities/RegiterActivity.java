package com.boomn.AccountModule.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.boomn.R;

import java.util.ArrayList;
import java.util.List;

public class RegiterActivity extends AppCompatActivity implements View.OnClickListener{

    Spinner sp_usertype;
    List<String> usertype_list = new ArrayList<>();
    TextView tv_spinneraction;
    ImageView iv_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();

    }

    private void initView() {
        tv_spinneraction = (TextView) findViewById(R.id.tv_spinner_action);
        tv_spinneraction.setOnClickListener(this);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        iv_close.setOnClickListener(this);
        sp_usertype = (Spinner) findViewById(R.id.sp_usertype);
        usertype_list.add("Agent /Broker");
        usertype_list.add("Builder");
        usertype_list.add("Buyer");
        usertype_list.add("General Public");
        ArrayAdapter<String> ad = new ArrayAdapter<String>(RegiterActivity.this,
                android.R.layout.simple_spinner_dropdown_item, usertype_list);

        sp_usertype.setAdapter(ad);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_spinner_action:
                sp_usertype.performClick();
                break;
            case R.id.iv_close:
                finish();
                break;
        }
    }
}
