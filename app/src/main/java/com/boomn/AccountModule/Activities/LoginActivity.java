package com.boomn.AccountModule.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.boomn.MainActivity;
import com.boomn.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_signup, action_login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
    }

    private void initViews() {
        tv_signup = (TextView) findViewById(R.id.tv_signup);
        tv_signup.setOnClickListener(this);
        action_login = (TextView) findViewById(R.id.action_login);
        action_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_signup:
                Intent signupintent = new Intent(LoginActivity.this, RegiterActivity.class);
                startActivity(signupintent);
                break;
            case R.id.action_login:
                Intent mainactivity = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(mainactivity);
                break;

        }
    }
}
