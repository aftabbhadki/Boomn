package com.boomn.AccountModule.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.boomn.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SuperSOSFragment extends Fragment {


    public SuperSOSFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_super_so, container, false);
        return view;
    }

}
